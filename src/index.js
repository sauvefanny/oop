import {Person} from './class/Person';

import {Counter} from './class/Counter';

let person1 = new Person('sauve', 'fanny', 32);
let person2 = new Person('bodu', 'bloup', 2);

console.log(person1.greeting());

document.body.appendChild(person2.toHTML());

let counter1 = new Counter (8) ; 
let counter2 = new Counter (2);

counter1.decrement();
counter2.increment();
console.log(counter1.value, counter2.value);