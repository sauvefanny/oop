export class Slider{
    loop = '';
    id = 0;
    constructor(loop){
        this.loop = loop;
    }
    loopFilm(){  
            let result = `
            <article class="movie-item">
            <img src="${this.loop[this.id].img}" alt="30" jours="" class="photo">
            <div class="item-info">
            <header>
            <h4>${this.loop[this.id].title}</h4>
            <h5 class="duration">Durée : ${this.loop[this.id].duration}</h5>
            </header>
            <p class="item-text">${this.loop[this.id].desc}</p>
            </div>
            </article>`;
            return result;
            
    }
    
    increment(){
        this.id+=1;
        this.loopFilm();
    }
    decrement(){
        this.id-=1;
        this.loopFilm();
    }
    
}