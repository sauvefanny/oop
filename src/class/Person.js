
export class Person {
    name;
    firstName;
    age;
    constructor(paraName, paraFirstName, paraAge) {
        this.name = paraName;
        this.firstName = paraFirstName;
        this.age = paraAge;
    }
    greeting() {
        return `hello my name is ${this.firstName} ${this.name} I am ${this.age} years old `;
    }
    toHTML() {
    const element = document.createElement('div');
    element.textContent = this.name +' '+ this.firstName;
    element.addEventListener('click', ()=>{
        alert(this.greeting());
    });
    return element;
}
}
