import { movie } from "./movieData"

import { Slider } from "./class/Slider"

const section = document.querySelector('.section-center');
let next = document.querySelector('#next');
let previous = document.querySelector('#previous');

function createFilm() {

    let slider1 = new Slider(movie);
    next.addEventListener('click', () => {
        if (slider1.id < slider1.loop.length - 1) {
            slider1.increment();
            section.innerHTML = slider1.loopFilm();
        }

    })
    previous.addEventListener('click', () => {
        if (slider1.id > 0) {
            slider1.decrement();
            section.innerHTML = slider1.loopFilm();
        }

    })
    section.innerHTML = slider1.loopFilm();

};

createFilm();

