export class Task {
    label;
    done = false;

    /**
     * @param {string} label 
     */
    constructor(label) {
        this.label = label;
    }
    /**
     * Méthode qui passe l'état done de true à false
     * ou de false à true
     */
    toggleDone() {
        if (this.done) {
            this.done = false;
        } else {
            this.done = true;
        }
        //ça fait pareil
        // this.done = !this.done;
    }
    toHTML() {
        let element = document.createElement('li');
        element.textContent = this.label;
        let checkbox = document.createElement('input');
        checkbox.type = 'checkbox';
        element.appendChild(checkbox);

        checkbox.checked = this.done;

        checkbox.addEventListener('change', ()=>{
            this.toggleDone();
        });
    };
}
/*
function Task(label) {
    this.label = label;
    this.done = false;
}


Task.prototype.toggleDone = function() {
    this.done = !this.done;
}
*/
