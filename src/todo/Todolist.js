import { Task } from "./Task";

export class TodoList {
    tasks;
    /**
     * @param {Task[]} tasks 
     */
    constructor(tasks = []) {
        this.tasks = tasks;
    }
    /**
     * Ajoute une nouvelle task dans la liste de trucs à faire
     * @param {string} label le label de la task à ajouter
     */
    addTask(label) {
        let instanceTask = new Task(label);
        this.tasks.push(instanceTask);
    }
    /**
     * Supprimer les tasks qui sont terminées
     */
    clearDone() {
        this.tasks = this.tasks.filter(item => !item.done);
    }
    /**
     * Méthode qui permet de passer une task de true à false ou de false à true
     * @param {number} index l'index de la task à cocher/décocher
     */
    toggleTask(index) {

        this.tasks[index].toggleDone();
    }
    /**
     * Méthode qui génère le HTML de la TodoList entière
     * @returns {HTMLElement}
     */
    toHTML() {
        const ul = document.createElement('ul');


        for (const item of this.tasks) {
           
            ul.appendChild(item.toHTML());
        }

        return ul;

    }
}
